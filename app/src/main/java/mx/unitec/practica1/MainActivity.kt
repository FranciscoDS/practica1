package mx.unitec.practica1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    private val TAG = "HolaMundo"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, msg: "OnStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, msg: "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, msg: "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, msg: "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, msg: "onDestroy")
    }

}